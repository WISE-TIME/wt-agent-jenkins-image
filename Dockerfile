FROM jenkins/jenkins:lts

# skip the setup wizard
ENV JAVA_ARGS -Djenkins.install.runSetupWizard=false
RUN echo 2.0 > /usr/share/jenkins/ref/jenkins.install.InstallUtil.lastExecVersion

# install minimum set of plugins
RUN install-plugins.sh \
  bouncycastle-api:2.16.2 \
  command-launcher:1.2 \
  job-dsl:1.69 \
  git:3.8.0 \
  workflow-aggregator:2.5 \
  startup-trigger-plugin:2.9.3

# create the seed job which spawns all other jobs
RUN mkdir -p /usr/share/jenkins/ref/jobs/seed-job
COPY seedJob.xml /usr/share/jenkins/ref/jobs/run-job-setup/config.xml
COPY *.groovy /usr/share/jenkins/ref/init.groovy.d/
