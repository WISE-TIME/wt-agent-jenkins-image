# WiseTime Agent Manager

Sets up Jenkins 2.x with the [Pipeline Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Pipeline+Plugin) to support the following scenarios:

 * All jobs are under version control and described via [Job-DSL](https://github.com/jenkinsci/job-dsl-plugin/wiki)
 * There is a [seed-job](seedJob.xml) which runs periodically to ensure the aforementioned jobs exist in Jenkins

## Run

Create the docker image and run it, with port 8080 forwarded to the host (set AGENT_TYPE to agent that you need):

```
$ docker build -t wt-agent .
$ docker run -it --rm -p 8080:8080 -e AGENT_TYPE=jira wt-agent
```

If you don't want to build own docker image you can use existing one:
```
$ docker run -it -p 8080:8080 -e AGENT_TYPE=jira practiceinsight/wt-agent:1.3
```

Once Jenkins is started you should see at least the init on [http://localhost:8080](http://localhost:8080).

## Cache Database

The agent jobs use database to cache internal data. Supported formats:

 * MySQL 5.7+;
 * MS SQL Server;
 * H2 embeded database.
 
### MySQL

You can launch MySQL docker image with following command:
```
docker run --name wt-agent-cache -p 3306:3306 -d -e MYSQL_ALLOW_EMPTY_PASSWORD=true \
	 -e MYSQL_USER=docker -e MYSQL_PASSWORD=docker -e MYSQL_DATABASE=agent_cache \
	 mysql:5.7 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
```
Following config have to be provided to wt-agent to connect to database:
```
cache_jdbc_url=jdbc:mysql://localhost:3306/agent_cache?useUnicode=true&characterEncoding=UTF-8&useSSL=false
cache_jdbc_username=docker
cache_jdbc_password=docker
```
For Windows or macOS url have to be: `jdbc:mysql://host.docker.internal:3306/agent_cache?useUnicode=true&characterEncoding=UTF-8&useSSL=false`

### MS SQL Server

Sample command to start MS SQL Server docker container ([more info](https://hub.docker.com/r/microsoft/mssql-server-linux/)):
```
docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=yourStrong(!)Password' -p 1433:1433 -d microsoft/mssql-server-linux:2017-CU6
```
Config for wt-agent to connect to database:
```
cache_jdbc_url=jdbc:sqlserver://localhost:1433
cache_jdbc_username=sa
cache_jdbc_password=yourStrong(!)Password
```
For Windows or macOS url have to be: `jdbc:sqlserver://host.docker.internal:1433`

### H2

H2 database can run in two mode:

 * In-memory. Sample URL: `jdbc:h2:mem:test;DB_CLOSE_DELAY=-1`
 * File persistent mode. Samle URL: `jdb:h2:file:/var/jenkins_home/h2_db/test;DB_CLOSE_DELAY=-1`

Sample config for wt-agent to use H2 database:

```
cache_jdbc_url=jdb:h2:file:/var/jenkins_home/h2_db/test;DB_CLOSE_DELAY=-1
cache_jdbc_username=docker
cache_jdbc_password=docker
```
If you planning to use file persistent mode, you can mount database folder to host machine (`docker run -v host_machine_folder:docker_container_folder`). E.g.:
```
docker run --name wt-agent -it -p 8080:8080 -e WT_AGENT=jira -v /hdd/docker/h2/wt-agent:/var/jenkins_home/h2_db ...rest-of-your-params
```

## Security

Default username is *admin*, default password is *admin*. You can change that by setting environment variables USERNAME and PASSWORD respectively. E.g.:

```
docker run -e USERNAME=myUser -e PASSWORD=securePassword ...
```

## Clean up

To save disk space builds older than 21 day will be automatically removed from history. You can change this by setting environment variable DISCARD_BUILD. E.g. for docker it will be:

```
docker run -e DISCARD_BUILD=7 ...
```

## How it works

It's a minimal Jenkins setup that runs in a docker container. Looking at the `Dockerfile` you can see that it:

 * Skips the setup wizard and leaves jenkins unsecured
 * Installs the minimum necessary [Job-DSL](https://wiki.jenkins-ci.org/display/JENKINS/Job+DSL+Plugin), [Git](https://wiki.jenkins-ci.org/display/JENKINS/Git+Plugin) and [Pipeline](https://wiki.jenkins-ci.org/display/JENKINS/Pipeline+Plugin) plugins
 * Copies the `seedJob.xml` to the Jenkins jobs directory
