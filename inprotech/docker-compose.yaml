# Copyright (c) 2018 Practice Insight Pty Ltd. All rights reserved.
#
# Docker Compose file for running WiseTime Agent Inprotech with a local MySQL cache.
# To start the WiseTime agent, run the following command in the same directory as this
# docker-compose.yaml file:
#
#   docker-compose up -d
#
# You can then access the agent's Jenkins UI at http://localhost:8080. To log in, use
# the credentials that you have set for the wt-agent service using the USERNAME and
# PASSWORD environment variables.
#
# To stop the WiseTime agent, run the following command in the same directory:
#
#   docker-compose down
#
# This docker-compose.yaml file creates two local volumes for persistence. To list the
# volumes:
#
#   docker volume ls
#
version: "3"
services:
  wt-agent:
    labels:
      description: WiseTime Agent Inprotech
    image: practiceinsight/wt-agent:1.3
    restart: always
    ports:
      - "8080:8080"
    environment:
      # specify the timezone to be used by the jenkins container (e.g. Australia/Perth), if not set, the default timezone is UTC.
      TZ: <timezone_name>

      USERNAME: admin
      PASSWORD: admin
      AGENT_TYPE: inprotech

      # paste your private service account key here
      agent_key_encoded: "YOUR_BASE64_ENCODED_API_KEY"
      cache_jdbc_url: "jdbc:mysql://wt-agent-cache:3306/agent_cache?useUnicode=true&characterEncoding=UTF-8&useSSL=false"
      cache_jdbc_username: docker
      cache_jdbc_password: docker
      inprotech_jdbc_url: "jdbc:sqlserver://HOST:PORT;databaseName=DATABASE_NAME;ssl=request;useCursors=true"
      inprotech_jdbc_username: USERNAME
      inprotech_jdbc_password: PASSWORD
      filesite_jdbc_url: "jdbc:sqlserver://HOST:PORT;databaseName=DATABASE_NAME;ssl=request;useCursors=true"
      filesite_jdbc_username: USERNAME
      filesite_jdbc_password: PASSWORD
      tag_modifier_activity_code_mapping: "Activity Name 1:code1,Activity Name 2:code2"
      default_activity_code: code1

      # enable aws logging
      CLOUD_LOG_ENABLED: "true"
    volumes:
      - wt-jenkins-home:/var/jenkins_home
      - ./.aws:/var/jenkins_home/.aws
  wt-agent-cache:
    labels:
      description: MySQL Cache for WiseTime Agent
    image: mysql:5.7
    restart: always
    # No need to expose MySQL Cache port; enable only for diagnostic use
    ## ports:
    ##   - "3306:3306"
    command: ["--character-set-server=utf8mb4", "--collation-server=utf8mb4_unicode_ci"]
    environment:
      # specify the timezone to be used by the jenkins container (e.g. Australia/Perth), if not set, the default timezone is UTC.
      TZ: <timezone_name>

      MYSQL_ALLOW_EMPTY_PASSWORD: "true"
      MYSQL_USER: docker
      MYSQL_PASSWORD: docker
      MYSQL_DATABASE: agent_cache
    volumes:
      - wt-mysql-data:/var/lib/mysql
volumes:
  wt-mysql-data: 
    driver: local
  wt-jenkins-home:
    driver: local
