import jenkins.model.*
import hudson.security.*
import javaposse.jobdsl.plugin.GlobalJobDslSecurityConfiguration

def instance = Jenkins.getInstance()
def username = System.getenv("USERNAME") ?: "admin"
def password = System.getenv("PASSWORD") ?: "admin"

def hudsonRealm = new HudsonPrivateSecurityRealm(false)
hudsonRealm.createAccount(username, password)
instance.setSecurityRealm(hudsonRealm)

def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
strategy.setAllowAnonymousRead(false);
instance.setAuthorizationStrategy(strategy)
instance.save()

GlobalConfiguration.all().get(GlobalJobDslSecurityConfiguration.class).useScriptSecurity=false
GlobalConfiguration.all().get(GlobalJobDslSecurityConfiguration.class).save()
