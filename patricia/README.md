# WiseTime Agent Setup

The WiseTime Agent is installed from a Docker image. When run it will download its executable components and start them inside of the Docker container. A MySQL container is used as a cache for the agent. We use [Docker Compose](https://docs.docker.com/compose/overview/) to configure and launch the containers.

## Configuration

Edit the `docker-compose.yaml` file and update the database connection credentials and other settings as needed.

## Launching via Docker Compose

To launch the WiseTime agent, navigate to the directory that contains the docker-compose.yaml file, and run:

```sh
docker-compose up -d
```

## Access Jenkins UI

The Jenkins UI can then be accessed from [http://localhost:8080](http://localhost:8080). Login credentials:

* Username: admin
* Password: admin

After logging in for the first time, go to the [run-job-setup job](http://localhost:8080/job/run-job-setup/) and click `Build Now`. This will download the WiseTime Agent executables into the Docker container. You can then start the following jobs by following the links and clicking on `Build Now`:

* [Detect New Tags (patricia)](http://localhost:8080/job/Detect%20New%20Tags%20(patricia)/)
* [Posted Time Listener (patricia)](http://localhost:8080/job/Posted%20Time%20Listener%20(patricia)/)

The agent will auto-update itself by running the [Agent Software Updater (patricia)](http://localhost:8080/job/Agent%20Software%20Updater%20(patricia)/) job once a day.

## Shutting down WiseTime Agent

To stop the WiseTime agent, run the following command in the directory that contains the docker-compose.yaml file:

```sh
docker-compose down
```

## Data Persistence

The Docker Compose configuration creates two local volumes for persistence. To list the volumes:

```sh
docker volume ls
```

The Jenkins volume is used to store the agent executables and logs. The MySQL volume is used to cache tag metadata when syncing cases to the WiseTime service. It is also used as a temporary cache as we download posted time logs from WiseTime and save diary entries to Patricia.